import "./App.css";
import React from "react";
import data from "./input.json";
import { EvenementCalendrier } from "./component";

function App() {
  return (
    <div className="App">
      <EvenementCalendrier events={data} />
    </div>
  );
}

export default App;
