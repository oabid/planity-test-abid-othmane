import React, { useState, useEffect } from "react";
import moment from "moment";

const EvenementCalendrier = ({ events }) => {
  const [width, setWidth] = useState(window.innerWidth);
  const [height, setHeight] = useState(window.innerHeight);

  /*

  UseEffect :

  La fonction useEffect est utilisée pour mettre à jour le composant lorsque la taille de la fenêtre change.
  Cela permet au composant de s'adapter automatiquement à des changements de taille de fenêtre,
  tels que les redimensionnements ou les changements de rotation de l'appareil.

  */
  useEffect(() => {
    const handleResize = () => {
      setWidth(window.innerWidth);
      setHeight(window.innerHeight);
    };
    window.addEventListener("resize", handleResize);
    return () => {
      window.removeEventListener("resize", handleResize);
    };
  }, []);

  const debut = 9;
  const fin = 21;
  const tempsTotal = (fin - debut) * 60; // en minutes

  const pixelParMinute = height / tempsTotal;

  /*

Le but de la fonction regroupementEvenements est de regrouper les evenements qui se chevauchent et les ranger par list.

On utilise la librairie moment qui nous sert à convertir les heures de début et de fin des événements en objets
Date JavaScript pour pouvoir ensuite calculer les minutes écoulées
entre ces deux heures.
Cela nous permet de calculer les positions et les tailles des éléments d'événements dans le calendrier en
utilisant des calculs basés sur les minutes plutôt que sur les heures.

*/
  const regroupementevenements = function (events) {
    let groups = [];
    events.forEach((event) => {
      let added = false;
      const debutEvenement = moment(event.start, "HH:mm").toDate();
      const finEvenement = moment(event.start, "HH:mm")
        .add(event.duration, "minutes")
        .toDate();
      groups.forEach((group) => {
        let overlapped = false;
        group.forEach((ev) => {
          const evStart = moment(ev.start, "HH:mm").toDate();
          const evEnd = moment(ev.start, "HH:mm")
            .add(ev.duration, "minutes")
            .toDate();
          if (evStart < finEvenement && evEnd > debutEvenement) {
            overlapped = true;
          }
        });
        if (overlapped) {
          group.push(event);
          added = true;
        }
      });
      if (!added) {
        groups.push([event]);
      }
    });

    console.log(groups);
    return groups;
  };

  const eventGroups = regroupementevenements(events);

  /*

  Cette fonction (arrow) permet de retourner le rendu en parcourant le tableau d'evenement grace à map
  Pour chaque événement, la constante calcule les positions et les tailles en utilisant les propriétés start,
  duration et width de l'événement, ainsi que les propriétés height, width de l'état du composant
  qui sont mise à jour lorsque la fenêtre est redimensionnée.

  */
  const eventElements = eventGroups.map((group) => {
    return group.map((event) => {
      const debutEvenement = moment(event.start, "HH:mm").toDate();
      const finEvenement = moment(event.start, "HH:mm")
        .add(event.duration, "minutes")
        .toDate();
      const debutEvenementMinutes =
        (debutEvenement.getHours() - debut) * 60 + debutEvenement.getMinutes();
      const finEvenementMinutes =
        (finEvenement.getHours() - debut) * 60 + finEvenement.getMinutes();
      const top = debutEvenementMinutes * pixelParMinute;
      const eventHeight =
        (finEvenementMinutes - debutEvenementMinutes) * pixelParMinute;
      const eventWidth = 100 / group.length;
      return (
        <div
          key={event.id}
          style={{
            position: "absolute",
            top,
            left: `${eventWidth * group.indexOf(event)}%`,
            width: `${eventWidth}%`,
            height: eventHeight,
            backgroundColor: "#f5f5f5",
            border: "1px solid #ccc",
            display: "flex",
            justifyContent: "center",
            alignItems: "center",
            borderRadius: "4px",
            backgroundImage:
              "linear-gradient(to right, #4CB8C4 0%, #3CD3AD  51%, #4CB8C4  100%)",
            boxShadow: "rgba(0, 0, 0, 0.24) 0px 7px 10px",
            color: "white",
            fontWeight: "bold",
          }}
        >
          {event.id}
        </div>
      );
    });
  });

  return (
    <div style={{ position: "relative", width: "100%", height: "100%" }}>
      {eventElements}
    </div>
  );
};

export default EvenementCalendrier;
